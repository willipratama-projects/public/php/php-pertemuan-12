## Tentang

Source Code Praktikum Pemrograman PHP (Pertemuan 12) AMIK Bumi Nusantara Cirebon

Membutuhkan:
- PHP 7+.
- [steampixel/simple-php-router](https://github.com/steampixel/simplePHPRouter).
- [symfony/dotenv](https://github.com/symfony/dotenv).

Source code ini hanya untuk pembelajaran, silakan mengembangkan sendiri PHP Mini Framework ini.
