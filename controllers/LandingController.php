<?php

namespace Controllers;

use Models\Dosen;

class LandingController
{
    function __construct() {
		parent::__construct();
	}

    public function index()
    {
        $title = 'Beranda - ' . $_ENV['APP_NAME'];
        $appTitle = $_ENV['APP_NAME'];
        $dosenList = Dosen::get();
        require(VIEW_PATH . 'pages/index.php');
    }
}
