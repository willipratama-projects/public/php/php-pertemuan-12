<?php
namespace Models;

class Dosen
{
    function get() {
        $value = array(
            array(
                'avatar' => 'willipratama.jpg',
                'name' => 'Willi Pratama',
                'facebook' => 'williTBA',
                'twitter' => 'willipratama',
                'instagram' => 'willi.ajah',
                'linkedin' => 'willipratama'
            ),
            array(
                'avatar' => 'female-person.jpg',
                'name' => 'Jane Doe',
                'facebook' => '',
                'twitter' => '',
                'instagram' => '',
                'linkedin' => ''
            ),
            array(
                'avatar' => 'male-person.jpg',
                'name' => 'John Doe',
                'facebook' => '',
                'twitter' => '',
                'instagram' => '',
                'linkedin' => ''
            ),
        );
        return $value;
    }
}
