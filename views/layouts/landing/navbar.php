<!-- Start Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-info sticky-top p-0">
    <div class="container">
        <a class="navbar-brand bg-white text-info p-3" href="/">
            <img src="images/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
            <?php echo $_ENV['APP_NAME']; ?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="fas fa-university text-warning"></i>&nbsp;&nbsp;Link 1
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="fas fa-book text-warning"></i>&nbsp;&nbsp;Link 2
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/profil">
                        <i class="fas fa-sign-in-alt text-warning"></i>&nbsp;&nbsp;Login
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->