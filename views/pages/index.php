<!DOCTYPE html>
<html lang="en">

<?php include_once(VIEW_PATH . 'layouts/landing/head.php'); ?>

<body>
    <?php include_once(VIEW_PATH . 'layouts/landing/navbar.php'); ?>
    <!-- Start main -->
    <main class="container my-4">
        <!-- Start Campus Card Info -->
        <div class="row py-2">
            <div class="col-lg-4 col-md-4 col-sm-12 py-2">
                <div class="card bg-primary text-white h-100">
                    <div class="card-body">
                        <i class="fas fa-university fa-3x"></i>
                        <p class="card-text pt-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Veritatis ut amet quis eveniet! Sint aliquid tempora amet, et aut illum.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 py-2">
                <div class="card bg-warning text-white h-100">
                    <div class="card-body">
                        <i class="fas fa-book fa-3x"></i>
                        <p class="card-text pt-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Veritatis ut amet quis eveniet! Sint aliquid tempora amet, et aut illum.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 py-2">
                <div class="card bg-danger text-white h-100">
                    <div class="card-body">
                        <i class="fas fa-money-bill fa-3x"></i>
                        <p class="card-text pt-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Veritatis ut amet quis eveniet! Sint aliquid tempora amet, et aut illum.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Campus Card Info -->
        <!-- Start Campus Info -->
        <div class="row py-2">
            <div class="col-lg-6 col-md-6 col-sm-12 py-2">
                <img src="images/campus3.png" class="img-fluid">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 py-2">
                <h2 class="text-info">About</h2>
                <h4 class="my-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, molestiae?</h4>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis, tenetur doloremque. Fuga nam quod repudiandae totam, ab culpa iusto maiores dolorum cumque, quibusdam neque tempora adipisci. Facere labore impedit doloribus quibusdam perspiciatis iste architecto possimus tenetur ratione magni non aperiam enim eius autem, deleniti nostrum quisquam esse veniam est aliquid.</p>
                <a href="#" class="btn btn-lg btn-warning"><strong>Read More</strong></a>
            </div>
        </div>
        <!-- End Campus Info -->
        <!-- Start Teacher List -->
        <div class="row">
            <div class="col text-center py-2">
                <hr>
                <h2 class="text-warning">Dosen Pengajar</h2>
            </div>
        </div>
        <div class="row">
            <?php foreach ($dosenList as $list): ?>
            <div class="col d-flex justify-content-center pb-4">
                <div class="card border border-info" style="width: 18rem;">
                    <img src="images/<?php echo (!empty($list['avatar'])) ? $list['avatar'] : 'person-placeholder-image.png'; ?>" class="card-img-top" alt="'<?php echo $list['name']; ?>'">
                    <div class="card-name bg-info text-white text-center rounded-pill mx-auto">
                        <h5 class="my-2"><?php echo $list['name']; ?></h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-3 d-flex justify-content-center">
                                <a href="<?php echo (!empty($list['facebook'])) ? 'https://web.facebook.com/' . $list['facebook'] : '#'; ?>" class="btn btn-primary rounded-circle"><i class="fab fa-facebook"></i></a>
                            </div>
                            <div class="col-3 d-flex justify-content-center">
                                <a href="<?php echo (!empty($list['twitter'])) ? 'https://twitter.com/' . $list['twitter'] : '#'; ?>" class="btn btn-info rounded-circle"><i class="fab fa-twitter"></i></a>
                            </div>
                            <div class="col-3 d-flex justify-content-center">
                                <a href="<?php echo (!empty($list['instagram'])) ? 'https://www.instagram.com/' . $list['instagram'] : '#'; ?>" class="btn btn-danger rounded-circle"><i class="fab fa-instagram"></i></a>
                            </div>
                            <div class="col-3 d-flex justify-content-center">
                                <a href="<?php echo (!empty($list['linkedin'])) ? 'https://www.linkedin.com/in/' . $list['linkedin'] : '#'; ?>" class="btn btn-primary rounded-circle"><i class="fab fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </div>
    </main>
    <!-- End main -->
    <?php include_once(VIEW_PATH . 'layouts/landing/footer.php'); ?>

    <?php include_once(VIEW_PATH . 'layouts/landing/script.php'); ?>
</body>

</html>